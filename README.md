# jp-vocab-anki

Here I save my new words that need to be added to Anki, as well as some resulting decks.

## Newest Finished Decks

A few of the neweset decks. For versions without pictures or also without audio, check the corresponding book or game below. Note that decks with a star emoji "⭐" have manually selected images. See [below](#all-images-deck-with-a-star-emoji-⭐) for more info.

* [走れ！Ｔ校バスケット部 ⭐](./decks/all-media/book-走れ！Ｔ校バスケット部-allmedia.apkg) (~490 words, [info](#走れｔ校バスケット部)) 
* [にわか魔女のタマユラさん ⭐](./decks/all-media/book-にわか魔女のタマユラさん-allmedia.apkg) (~70 words, [info](#にわか魔女のタマユラさん))
* [英語ができない私をせめないで！ ⭐](./decks/all-media/book-英語ができない私をせめないで！-allmedia.apkg) (~115 words, [info](#英語ができない私をせめないで-again))
* [ガールズ・ブルー ⭐](./decks/all-media/book-ガールズ・ブルー-allmedia.apkg) (~600 words, [info](#ガールズ・ブルー))
* [祟りのゆかりちゃん (⭐)](./decks/all-media/book-祟りのゆかりちゃん-allmedia.apkg) (~835 words, [info](#祟りのゆかりちゃん))



## List of Contents

[[_TOC_]]



# Please Note, and Workflow
Sometimes the words I mark in a book as unknown can be duplicates within the book and within the collection, as I often take some time between reading a book and getting the words into Anki. For a more detailed breakdown all the way from reading a new word to having it as a finished Anki card, read on.

## Book Selection and Reading Tips
The first few books I had were all bought from a random second-hand shop while I was abroad, not in Japan, in 2016.
I had only been studying Japanese for less than a year so I spent some time looking through all the books and trying to find ones that I expected to be able to read at least soon enough, but also some variation in difficulty and themes.
They were all very cheap, but since I could only carry a limited amount of books while abroad, I bought around 5-10 books and read two of them that seemed to be the most approachable ones pretty soon after buying them, while still abroad.
Reading these two books felt extremely tedious since I wrote every word onto sticky notes, instead of just marking the words in the books.
But since I had nothing else to do while commuting to the job I had for a while back then, it was easy to push through.

Then after not reading a single Japanese book for around four years, a short hiatus from studying Japanese and some general progress, I started reading more, this time marking words directly in a book and strictly not looking up words during reading.
And this is the single best advice I can give to anyone trying to learn a language by reading books:
**Mark the words you don't know directly in the book, so you don't waste time while reading but so you can still look up all the words you want later. Also do not look up any words during reading, just keep on reading. The only exception should be key words that are repeated a lot of times or carry some other special meaning throughout the (current part of) the story.**
For games or other digital media try to take screenshots, ideally with the word highlighted.
With this you avoid feeling like reading a book is work.
In the end you can just decide to ignore the markings in a book, so not put them into Anki or anything, without any hard feelings, since it barely took you any time to mark the words anyway.
If you want to be extra nice to your future self, really only mark the word you don't know, not the whole line or any parts of the words before or after.
This makes it easier later to just get the words without reading the stuff around it.
But then again be mindful that often it can look like it is just a single word that you don't understand, but in fact it is part of an expression you don't yet know.
Most digital dictionaries will need some finessing to give you possible expression, such as wildcard characters like the asterisk \* or just trying to put more of the words in.

Once I finished most of the old books I had, I got some more online. Locally I can't get any here in Germany's south, so I got all of them online, from [takagi-books.de](https://www.takagi-books.de/wordpress/produkt-kategorie/%e6%96%87%e5%ba%ab) and [verasia.de](https://verasia.de/14-lesebuecher?orderby=reference&orderway=desc&orderway=desc).
There I usually just check if the brief story description is at least somewhat understandable and if the reviews are good enough.
For this I just check some results for 'review 書名' in Google, with '書名' replaced by the book title.


## Workflow for a single book
This write up is for anyone interested in how the Anki cards are created. Also for me in case I take a hiatus and forget something.

### Reading
When I read a book, I use a pen to mark all the words that I want to look up later. Just a line exactly next to the word I might want to check later. Reading should not feel like work, so I just mark the words and don't look them up. Only in rare cases when I feel like it is detrimental to understanding the story if I don't look it up right now.

### Extracting Words
Usually when I am already two books further, rarely earlier, I will go through the book and write down all the marked words. For a few of the first few books I just wrote them into the text file without checking a dictionary for the base form of the word or if it is in fact not a whole word or two words together or something. This resulted in some jank word lists, which also lead to some confusing Anki cards. For later books (approximately from [ズッコケ三人組の卒業式](#ズッコケ三人組の卒業式) onwards) I checked a dictionary first, usually [jisho.org](https://jisho.org/). Also at first I didn't always keep the way the word was written in the book, but just wrote down the dictionary form. Now the word lists have each word in the form they appear in the book and next to it written in all Kana. This way the Anki cards will have the same writing of the word as the book.

#### From Word to Anki Deck
For the rest of the process, check the section '[Anki Deck Creation](#anki-deck-creation)' below.


# Collection


## Books

Most of the books I have read, sorted by the order in which I read them. For most info, like release year and number of pages, I looked it up on the internet, not from the books themselves.

### 英語ができない私をせめないで！
![Cover of the book 英語ができない私をせめないで！](/books/cover/semenaide.jpg "Cover of the book 英語ができない私をせめないで！")  
**Title:** 英語ができない私をせめないで！  
**Subtitle:** I want to speak English!  
**Author:** 小栗 左多里 (Oguri Saori)  
**ISBN:** 978-4479300229 / 4479300228  
**Publisher:** 大和書房 (Daiwashobou)  
**Release Year:** 2006  
**Pages:** 217 (215)  

*Blurb:* ある日、「英語を勉強してみよう！」と思った。無謀にも、「have」って何?そこから英語を学んでいこうという著者。英語の本を買ってみた、英会話スクールにも通ってみた、ネットで勉強もしてみた、ラジオ英会話も聴いてみた……。二ヵ月坊主だった私が出した結論！著者の「英語最強メニュー」はこれしかありません！

<a href="books/pages/semenaide0.png" target="_blank"><img src="books/pages/semenaide0.jpg" alt="Page scan of the book 英語ができない私をせめないで！" width="500rem" height="auto"></a>
<a href="books/pages/semenaide1.png" target="_blank"><img src="books/pages/semenaide1.jpg" alt="Page scan of the book 英語ができない私をせめないで！" width="500rem" height="auto"></a>
<a href="books/pages/semenaide2.png" target="_blank"><img src="books/pages/semenaide2.jpg" alt="Page scan of the book 英語ができない私をせめないで！" width="500rem" height="auto"></a>

*My Thoughts:* (Written seven years after reading the book)  
**5/10 reading difficulty**.  
Most **unknown words are from a language learning context and everyday stuff**.  
**No Furigana, only for rare words**.  
Mostly easy to read with many english words in Katakana. The book is separated into a few big sections of language learning, which each contain many tiny chapters of the language learning journey of the protagonist.
There is no tense plot or anything, just someone who tries different approaches, with a lot of humor and many pictures, occasionally even a handfull of manga-like pages. Definitely easy to follow, even with some rarer words.  
**How happy am I that I read this?: 7/10**

Note: I reread this book a few years later, see [below](#英語ができない私をせめないで-again) for words and anki decks!

*Words:* --- (not collected in a separate file)  
*Decks:* --- (not saved in a separate deck)

* * * * * * * * * *

### こちらズッコケ探偵事務所
![Cover of the book こちらズッコケ探偵事務所](/books/cover/detectives.jpg "Cover of the book こちらズッコケ探偵事務所")  
**Title:** こちらズッコケ探偵事務所  
**Author:** 正幹 那須 (Masamoto Nasu)  
**ISBN:** 978-4591023495 / 4591023494  
**Publisher:** ポプラ社 (POPLAR)  
**Release Year:** 1986  
**Pages:** 206 (197)  

*Blurb:* ブタのぬいぐるみにかくされた重大な秘密をめぐっておこる怪事件。おなじみモーちゃん、ハカセ、ハチベエのズッコケ探偵大活躍。

<a href="books/pages/detectives1.png" target="_blank"><img src="books/pages/detectives1.jpg" alt="Page scan of the book こちらズッコケ探偵事務所" width="500rem" height="auto"></a>
<a href="books/pages/detectives2.png" target="_blank"><img src="books/pages/detectives2.jpg" alt="Page scan of the book こちらズッコケ探偵事務所" width="500rem" height="auto"></a>

*My Thoughts:* **4/10 reading difficulty**. (Written a few years after reading the book).  
Most unknown words or expressions are easy to understand from context. Lots of **school related words**. Seems to be aimed at middle schoolers.  
**No Furigana for super basic elementary school words, anything harder than that with Furigana or without Kanji at all.**  
It is easy to read, no huge metaphors or anything, but rather **many casual interjections and expressions that you won't easily find** in textbooks. Occasionally pictures, sized from tiny ones to page-sized ones, help in more easily understanding the plot. Most of the book consists of dialogues or narration around the dialogues. The rest of the narration is just description from a third person narrator who does not have any further backgrund info.
The story is, just as the other book from this series that I've read, about the three main protagonists and a slightly dumb idea and some bad happenstance that turns into a childish kidnapping. This one seems to be the 8th of the 50 books of this series.  
**How happy am I that I read this?: 5/10**

*Words:* [~480](./words/converted/こちらズッコケ探偵事務所.txt)  
*Decks:* [No Media](./decks/no-media/book-こちらズッコケ探偵事務所-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-こちらズッコケ探偵事務所-nopictures.apkg) | [All Media](./decks/all-media/book-こちらズッコケ探偵事務所-allmedia.apkg)

* * * * * * * * * *

### 車いすのパティシエ
![Cover of the book 車いすのパティシエ](/books/cover/patissier.jpg "Cover of the book 車いすのパティシエ")  
**Title:** 車いすのパティシエ  
**Subtitle:** 涙があふれて　心が温かくなる話  
**Author:** ニッポン放送 (Nippon Broadcasting System)  
**ISBN:** 978-4594052386 / 459405238X   
**Publisher:** 扶桑社 (FUSOSHA)  
**Release Year:** 2006  
**Pages:** 167  

*Blurb:* 表題作を含むニッポン放送ラジオ番組「１０時のちょっといい話」のパーソナリティうえやなぎまさひこ氏が過去に放送した約１０００話の中から厳選した心温まる実話２３編を収録。両脚を切断した後もケーキ作りに励む「車いすのパティシェ」、父が息子に贈ったお墓「天国への郵便ポスト」、大阪大空襲の被害から人々を守った「命を運んだ救援電車」など。ラジオファンも、ラジオを聞かない人にも読んでほしい１冊。朝１０時はニッポン放送を聴こう。

<a href="books/pages/patissier0.png" target="_blank"><img src="books/pages/patissier0.jpg" alt="Page scan of the book 車いすのパティシエ" width="500rem" height="auto"></a>
<a href="books/pages/patissier1.png" target="_blank"><img src="books/pages/patissier1.jpg" alt="Page scan of the book 車いすのパティシエ" width="500rem" height="auto"></a>
<a href="books/pages/patissier2.png" target="_blank"><img src="books/pages/patissier2.jpg" alt="Page scan of the book 車いすのパティシエ" width="500rem" height="auto"></a>

*My Thoughts:* **8/10 reading difficulty**. (Written a few years after reading the book).  
Many difficult words from very different fields.  
**No Furigana, not even for names, only for extremely rare or specialized words.**  
The book consists of a collection of the best shows of a late night radio, so they are completely different chapters telling the stories of different persons. This leads to new rare words for each of the chapters, which can be very taxing when starting a chapter.
A lot of it is just the radio host explaining something, and then often there is a guest of the story who he has some kind of very focused conversation with. Occasionally page-sized pictures illustrate a story.  
**How happy am I that I read this?: 7/10**

*Words:* first third not collected in a separate file, other two thirds [~430](./words/converted/車いすのパティシエ.txt)  
*Decks:* [No Media](./decks/no-media/book-車いすのパティシエ-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-車いすのパティシエ-nopictures.apkg) | [All Media](./decks/all-media/book-車いすのパティシエ-allmedia.apkg)

* * * * * * * * * *

### くちぶえ番長
![Cover of the book くちぶえ番長](/books/cover/whistler.jpg "Cover of the book くちぶえ番長")  
**Title:** くちぶえ番長  
**Author:** 重松 清 (Shigematsu Kiyoshi)  
**ISBN:**  978-4101349206 / 4101349207  
**Publisher:** 新潮社 (SHINCHOSHA)  
**Release Year:** 2007  
**Pages:** 236 (231)  

*Blurb:* 小学四年生のツヨシのクラスに、一輪車とくちぶえの上手な女の子、マコトがやってきた。転校早々「わたし、この学校の番長になる!」と宣言したマコトに、みんなはびっくり。でも、小さい頃にお父さんを亡くしたマコトは、誰よりも強く、優しく、友だち思いで、頼りになるやつだったんだ―。サイコーの相棒になったマコトとツヨシが駆けぬけた一年間の、決して忘れられない友情物語。

<a href="books/pages/whistler0.png" target="_blank"><img src="books/pages/whistler0.jpg" alt="Page scan of the book くちぶえ番長" width="250rem" height="auto"></a>
<a href="books/pages/whistler1.png" target="_blank"><img src="books/pages/whistler1.jpg" alt="Page scan of the book くちぶえ番長" width="500rem" height="auto"></a>
<a href="books/pages/whistler2.png" target="_blank"><img src="books/pages/whistler2.jpg" alt="Page scan of the book くちぶえ番長" width="500rem" height="auto"></a>

*My Thoughts:* **4/10 reading difficulty**. (Written one year after reading the book).  
Most unknown words or expressions are easy to understand from context. Seems to be aimed at middle schoolers.  
**No Furigana for basic words, intermediate words with Furigana.**  
Easy to read, some **casual interjections and expressions that you won't easily find** in textbooks. Occasionally pictures, sized from tiny ones to page-sized ones, help in more easily understanding the story. Most of the book is narration from the protagonist about what is happening, with many dialogues.
The story is about a boy in school who meets the nonchalant and self-confident transfer student Makoto and starts to grow fond of her and her character. The chapters are rather episodic but steadily develop the relationship between the characters.  
**How happy am I that I read this?: 8/10**

*Words:* [~240](./words/converted/くちぶえ番長.txt)  
*Decks:* [No Media](./decks/no-media/book-くちぶえ番長-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-くちぶえ番長-nopictures.apkg) | [All Media](./decks/all-media/book-くちぶえ番長-allmedia.apkg)

* * * * * * * * * *

### トンカチと花将軍
![Cover of the book トンカチと花将軍](/books/cover/general.jpg "Cover of the book トンカチと花将軍")  
**Title:** トンカチと花将軍  
**Author:** 舟崎 克彦, 舟崎 靖子 (Funazaki Yoshihiko, Funazaki Yasuko)  
**ISBN:** 978-4834018110 / 4834018113  
**Publisher:** 福音館書店 (FUKUINKAN SHOTEN)  
**Release Year:** 2002  
**Pages:** 220 (214)  

*Blurb:* 犬のサヨナラを追って森の奥へはいっていった少年、トンカチは、「あねもね館」に住む「将軍」とシャム猫のヨジゲン、あらいぐまのトマトなどの奇妙な連中に出会います。みんな親切にサヨナラをさがしてくれるのですが、おかしな事件に次々とまきこまれて…。ユーモアあふれるファンタジーです。

<a href="books/pages/general0.png" target="_blank"><img src="books/pages/general0.jpg" alt="Page scan of the book トンカチと花将軍" width="500rem" height="auto"></a>
<a href="books/pages/general1.png" target="_blank"><img src="books/pages/general1.jpg" alt="Page scan of the book トンカチと花将軍" width="500rem" height="auto"></a>
<a href="books/pages/general2.png" target="_blank"><img src="books/pages/general2.jpg" alt="Page scan of the book トンカチと花将軍" width="500rem" height="auto"></a>

*My Thoughts:* **5/10 reading difficulty**. (Written one year after reading the book).  
Most unknown words or expressions are easy to understand from context. Lots of **nature related words or rural words**, like names of plants and animals. Seems to be aimed at middle schoolers.  
**No Furigana for super basic elementary school words, anything harder than that with Furigana or without Kanji at all.**  
It is easy to read, no huge metaphors or anything, but rather **many casual interjections and expressions that you won't easily find** in textbooks. Occasionally pictures, sized from tiny ones to page-sized ones, help in more easily understanding the plot.
Most of the book consists of the protagonist trying to find something and encountering wacky characters, with many dialogues. The story is about a boy who runs after his dog into the woods and finds himself transported into a place where animals can talk and go after their daily lives. There some kinda episodic stuff happens.
The story is nothing special, kinda like a mix of *Winnie Pooh* and *Spirited Away*.  
**How happy am I that I read this?: 5/10**

*Words:* [~470](./words/converted/トンカチと花将軍.txt)  
*Decks:* [No Media](./decks/no-media/book-トンカチと花将軍-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-トンカチと花将軍-nopictures.apkg) | [All Media](./decks/all-media/book-トンカチと花将軍-allmedia.apkg)

* * * * * * * * * *

### ガールズ・ブルー
![Cover of the book ガールズ・ブルー](/books/cover/blue.jpg "Cover of the book ガールズ・ブルー")  
**Title:** ガールズ・ブルー  
**Author:** あさの あつこ (Asano Atsuko)  
**ISBN:** 978-4167722012 / 4167722011  
**Publisher:** 文藝春秋 (Bungeishunju)  
**Release Year:** 2006  
**Pages:** 231 (218)  

*Blurb:* 落ちこぼれ高校に通う理穂、美咲、如月。十七歳の誕生日を目前に理穂は失恋。身体が弱く入院を繰り返す美咲は同情されるのが大嫌い。如月は天才野球選手の兄・睦月と何かと比較される。でもお構いなしに、それぞれの夏は輝いていた。葛藤しながら自分自身を受け入れ愛する心が眩しい、切なくて透明な青春群像小説。

<a href="books/pages/blue1.png" target="_blank"><img src="books/pages/blue1.jpg" alt="Page scan of the book ガールズ・ブルー" width="500rem" height="auto"></a>
<a href="books/pages/blue2.png" target="_blank"><img src="books/pages/blue2.jpg" alt="Page scan of the book ガールズ・ブルー" width="500rem" height="auto"></a>

*My Thoughts:* **6/10 reading difficulty**. (Written one year after reading the book).  
Most unknown words are **casual youth or fashion language**. Most unknown words are understandable from context or can be ignored. Seems to be aimed at female high schoolers.  
**Occasionally Furigana, mainly for advanced words**.  
Mostly easy to read, sometimes the characters use confusing metaphors or are moody, which can be hard to get immediately as a Japanese learner. But in the end, most of it was clear without any effort, if you can wait for a few lines before getting it.  
The plot of the story did not stick with me at all almost a year later. I just remember that some of it was about about romantic relationships, some of it about future dreams and career plans, some of it just about growing up and friends. Superficially it tries to be emotional and existential.  
**How happy am I that I read this?: 4/10**

*Words:* [~830](./words/converted/ガールズ・ブルー.txt)  
*Decks:* (~600 words) [No Media](./decks/no-media/book-ガールズ・ブルー-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-ガールズ・ブルー-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-ガールズ・ブルー-allmedia.apkg)

* * * * * * * * * *

### 信太郎くんの伝道日記
![Cover of the book 信太郎くんの伝道日記](/books/cover/missionary.jpg "Cover of the book 信太郎くんの伝道日記")  
**Title:** 信太郎くんの伝道日記  
**Subtitle:** 肯定的人生論  
**Author:** 関口 淳次 (Sekiguchi Junji)  
**Release Year:** 2000  
**Pages:** 115 (111)  

<a href="books/pages/missionary0.png" target="_blank"><img src="books/pages/missionary0.jpg" alt="Page scan of the book 信太郎くんの伝道日記" width="500rem" height="auto"></a>
<a href="books/pages/missionary1.png" target="_blank"><img src="books/pages/missionary1.jpg" alt="Page scan of the book 信太郎くんの伝道日記" width="500rem" height="auto"></a>
<a href="books/pages/missionary2.png" target="_blank"><img src="books/pages/missionary2.jpg" alt="Page scan of the book 信太郎くんの伝道日記" width="500rem" height="auto"></a>

*My Thoughts:* **5/10 reading difficulty**. (Written half a year after reading the book).  
Most unknown words or expressions are understandable from context. Lots of **religion related words**, sometimes rather rare ones. Seems to be aimed at yound adults.  
**Furigana only very rarely.**  
I can't find this book anywhere to buy online and there is no publisher in the book. Looks like the author published this himself with the help of his church group and some book printing company.
The book consists of episodic chapters, each just a handfull or two of pages. Easy to read, occasionally with page-sized pictures.
Most of the book consists of dialogues between the protagonist and some main person of the chapter, who often has some problem and then receives some christian wisdom from the protagonist. In some rare chapters, there is no other person other than the protagonist. The narration is from the perspective of the protagonist.  
**How happy am I that I read this?: 5/10**

*Words:* [~250](./words/converted/%E4%BF%A1%E5%A4%AA%E9%83%8E%E3%81%8F%E3%82%93%E3%81%AE%E4%BC%9D%E9%81%93%E6%97%A5%E8%A8%98.txt)  
*Decks:* [No Media](./decks/no-media/book-信太郎くんの伝道日記-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-信太郎くんの伝道日記-nopictures.apkg) | [All Media](./decks/all-media/book-信太郎くんの伝道日記-allmedia.apkg)

* * * * * * * * * *

### １００億人のヨリコさん
![Cover of the book １００億人のヨリコさん](/books/cover/yoriko.jpg "Cover of the book １００億人のヨリコさん") ![Old (and better) cover of the book １００億人のヨリコさん](/books/cover/yoriko-old.jpg "Old (and better) cover of the book １００億人のヨリコさん")  
**Title:** １００億人のヨリコさん  
**Author:** 似鳥 鶏 (Nitadori Kei)  
**ISBN:** 978-4334778583 / 4334778585  
**Publisher:** 光文社 (Kobunsha)  
**Release Year:** 2019  
**Pages:** 331 (318)  

*Blurb:* 貧乏極まり行き場を失くした小磯は、学生課で寮費千三百円という怪しげな「富穣寮」を紹介される。大学キャンパスの奥の奥。そこでは、変人の寮生たちが奇妙な自給生活を繰り広げていた。しかも部屋には、夜な夜なヨリコさんという「血まみれの女」が現れるという。ヨリコさんの正体を解き明かそうとする小磯は、やがて世界の存続をかけた戦いに巻き込まれていく!

<a href="books/pages/yoriko1.png" target="_blank"><img src="books/pages/yoriko1.jpg" alt="Page scan of the book １００億人のヨリコさん" width="500rem" height="auto"></a>
<a href="books/pages/yoriko2.png" target="_blank"><img src="books/pages/yoriko2.jpg" alt="Page scan of the book １００億人のヨリコさん" width="500rem" height="auto"></a>

*My Thoughts:* **8/10 reading difficulty**. (Written half a year after reading the book).  
Most unknown words or expressions are understandable from context or not really important. All of the protagonists have very different personalities that also affect their language. One of them is using samurai-like arachic language for example. Seems to be aimed at young college students.  
**No Furigana, only for rare words or for words that are usually written using kana.**  
I guess the book is a **horror thriller**, since it is about a mysterious horror phenomenon plaguing a group of people, which the main protagonist tries to understand with scientific reasoning. This leads to some situations that are hard to understand just from context immediately. Most of the narration is the thoughts of the main protagonist and therefore from his perspective, without any extra knowledge about other people or any other background info. The first half of the story is basic foundation laying with a few enjoyable twists and mysteries. The second half gets increasingly captivating with a good ending.  
**How happy am I that I read this?: 7/10**

*Words:* [~1100](./words/converted/１００億人のヨリコさん.txt) <!-- *Decks:* [No Media](./decks/no-media/book-１００億人のヨリコさん-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-１００億人のヨリコさん-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-１００億人のヨリコさん-allmedia.apkg) -->   
*Decks:* (~1000 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### ズッコケ三人組の卒業式
![Cover of the book ズッコケ三人組の卒業式](/books/cover/graduation.jpg "Cover of the book ズッコケ三人組の卒業式")  
**Title:** ズッコケ三人組の卒業式  
**Author:** 正幹 那須 (Masamoto Nasu)  
**ISBN:** 978-4591085783 / 4591085783  
**Publisher:** ポプラ社 (POPLAR)  
**Release Year:** 2005  
**Pages:** 208 (196)  

*Blurb:* 三人組も卒業式を間近にひかえて行事続きだったが、そもそも自分たちのタイムカプセルをうめようとしたのが、事件へ発展。無事卒業できるのか！

<a href="books/pages/graduation0.png" target="_blank"><img src="books/pages/graduation0.jpg" alt="Page scan of the book ズッコケ三人組の卒業式" width="500rem" height="auto"></a>
<a href="books/pages/graduation1.png" target="_blank"><img src="books/pages/graduation1.jpg" alt="Page scan of the book ズッコケ三人組の卒業式" width="500rem" height="auto"></a>
<a href="books/pages/graduation2.png" target="_blank"><img src="books/pages/graduation2.jpg" alt="Page scan of the book ズッコケ三人組の卒業式" width="500rem" height="auto"></a>

*My Thoughts:* **3/10 reading difficulty**. (Written half a year after reading the book).  
Most unknown words or expressions are easy to understand from context. Lots of **school related words**. Seems to be aimed at middle schoolers.  
**No Furigana for super basic elementary school words, anything harder than that with Furigana or without Kanji at all.**  
It is easy to read, no huge metaphors or anything, but rather **many casual interjections and expressions that you won't easily find** in textbooks. Occasionally pictures, sized from tiny ones to page-sized ones, help in more easily understanding the plot.
Most of the book consists of dialogues or narration around the dialogues. The rest of the narration is just description from a third person narrator who does not have any further backgrund info.
The story is, just as the other book from this series that I've read, about the three main protagonists and a slightly dumb idea and some bad happenstance that turns into a childish kidnapping. This one seems to be the last one of the 50 books of this series.  
**How happy am I that I read this?: 4/10**

*Words:* [~380](./words/converted/%E3%82%BA%E3%83%83%E3%82%B3%E3%82%B1%E4%B8%89%E4%BA%BA%E7%B5%84%E3%81%AE%E5%8D%92%E6%A5%AD%E5%BC%8F.txt)  
*Decks:* [No Media](./decks/no-media/book-ズッコケ三人組の卒業式-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-ズッコケ三人組の卒業式-nopictures.apkg) | [All Media](./decks/all-media/book-ズッコケ三人組の卒業式-allmedia.apkg)

* * * * * * * * * *

### 二度のお別れ
![Cover of the book 二度のお別れ](/books/cover/farewell.jpg "Cover of the book 二度のお別れ")  
**Title:** 二度のお別れ  
**Author:**  黒川 博行 (Kurokawa Hiroyuki)  
**ISBN:** 978-4041059425 / 4041059429  
**Publisher:** 角川 (KADOKAWA)  
**Release Year:** 2017  
**Pages:** 223 (212)  

*Blurb:* 三協銀行新大阪支店で強盗事件が発生。犯人は現金約400万円を奪い、客のひとりを拳銃で撃って人質として連れ去った。大阪府警捜査一課が緊急捜査を開始するや否や、身代金1億円を要求する脅迫状が届く。「オレワイマオコツテマス―」。脅迫状には切断された指が同封されていた。刑事の黒田は、相棒の“マメちゃん”こと亀田刑事とともに、知能犯との駆け引きに挑む。『破門』の直木賞作家のデビュー作にして圧巻の警察ミステリ。

<a href="books/pages/wakare1.png" target="_blank"><img src="books/pages/wakare1.jpg" alt="Page scan of the book 二度のお別れ" width="500rem" height="auto"></a>
<a href="books/pages/wakare2.png" target="_blank"><img src="books/pages/wakare2.jpg" alt="Page scan of the book 二度のお別れ" width="500rem" height="auto"></a>

*My Thoughts:* **8/10 reading difficulty**.  
A lot of **use of Osaka dialect whenever characters are speaking**. At least to me it was hard at first, since this is the first time I saw it. First **crime fiction** I read, many unknown words are from the police and crime context. Seems to be aimed at adults.  
**No Furigana, only for extremely uncommon words or for words that are usually written using kana.**  
Most of the unknown words were understandable from context, and many parts where I had trouble with words, didn't feel much harder to understand because of the words themselves, but rather because sometimes what happens or what is described is a bit complicated itself.
The story focuses on one bank robbery that happens very early and the resulting investigations of the regional police force, where we mainly follow one of the detectives and his partner. Most dialogue is very focused on the case, just like the whole story, which takes no digressions at all.
Once I got used to the Osaka dialect and the crime vocabulary, it was a thrilling read and mostly enganging throughout. It felt like just the right amount of pages and surprises for the case and especially the conclusion at the end was worth the read. The character's personalities themselves were very believable, even though at first they felt a bit tacked on.  
**How happy am I that I read this?: 8/10**

*Words:* [~1000](./words/converted/二度のお別れ.txt) <!-- *Decks:* [No Media](./decks/no-media/book-二度のお別れ-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-二度のお別れ-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-二度のお別れ-allmedia.apkg) -->   
*Decks:* (~925 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### 祟りのゆかりちゃん
![Cover of the book 祟りのゆかりちゃん](/books/cover/cursed.jpg "Cover of the book 祟りのゆかりちゃん")  
**Title:** 祟りのゆかりちゃん  
**Author:** 蒲原 二郎 (Kanbara Jirou)  
**ISBN:** 978-4344421790 / 4344421795  
**Publisher:** 幻冬舎 (GENTOSHA)  
**Release Year:** 2014  
**Pages:** 386 (373)  

*Blurb:* 六本木の片隅にある寺で働く由加里は、一生懸命だけど空回りしがちな女の子。おかげで就活に失敗し、顔はいいが口と性格の悪い住職にこき使われる毎日。さらにひょんなことから一生彼氏ができない祟りまでしょいこむハメに…。これを逃れるには百八人の悩みを解決しなければならないが、はたして由加里にでキるのか!?仏閣系青春コメディー!

<a href="books/pages/yukari0.png" target="_blank"><img src="books/pages/yukari0.jpg" alt="Page scan of the book 祟りのゆかりちゃん" width="250rem" height="auto"></a>
<a href="books/pages/yukari1.png" target="_blank"><img src="books/pages/yukari1.jpg" alt="Page scan of the book 祟りのゆかりちゃん" width="500rem" height="auto"></a>
<a href="books/pages/yukari2.png" target="_blank"><img src="books/pages/yukari2.jpg" alt="Page scan of the book 祟りのゆかりちゃん" width="500rem" height="auto"></a>

*My Thoughts:* **7/10 reading difficulty**.  
Most **unknown words are either from the temple context of the story, or felt like casual language common for young adults**, such as the two main protagonists. The temple related words can be a bit taxing at first and some get repeated a lot, but once those are looked up, the rest is often understandable from context. Seems to be aimed at high schoolers.  
**No Furigana, only for slightly rarer words or non-standard writing of words**.  
Mostly easy to read, but for some parts I didn't quite understand what was happening until a good few lines later. But in the end, everything was clear without any effort, if you can wait for a few lines before getting it. This was usually due to unknown or misinterpreted words.
The five chapters are very varied in the content and the words and turned out to be less episodic than the title premise from the first chapter suggested them to be. We always follow the protagonist Yukari and the few unusual adventures she finds herself in due to the title premise.
Not sure about the genre, but this seems to be aimed especially at female high schoolers, maybe this is what is called a 少女小説 (**girl's novel**). Compared to the only other novel of this type that I read so far (see [ガールズ・ブルー](#ガールズ・ブルー), where the main protagonists are actually still in high school, other than Yukari, who is fresh out of there), this one had a lot more action and tension, and also felt less emotional and existential, which made it a light-hearted read for most of the book. Yukari is not taken seriously by some of the older protagonists and she sees herself as clumsy and a little slow, but outside of her actions that led to her curse, this was never used as a cheap part of the narrative.  
**How happy am I that I read this?: 8/10**

*Words:* [~1000](./words/converted/祟りのゆかりちゃん.txt)  
*Decks:* (~835 words) [No Media](./decks/no-media/book-祟りのゆかりちゃん-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-祟りのゆかりちゃん-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-祟りのゆかりちゃん-allmedia.apkg)

* * * * * * * * * *

### 走れ！Ｔ校バスケット部
![Cover of the book 走れ！Ｔ校バスケット部](/books/cover/basketball.jpg "Cover of the book 走れ！Ｔ校バスケット部")  
**Title:** 走れ！Ｔ校バスケット部  
**Author:** 松崎 洋 (Matsuzaki Hiroshi)  
**ISBN:** 978-4344414433 / 4344414438  
**Publisher:** 幻冬舎 (GENTOSHA)  
**Release Year:** 2010  
**Pages:** 258 (247)  

*Blurb:* 中学時代、バスケ部キャプテンとして関東大会二位の実績を残した陽一は、強豪私立H校に特待生として入学。だが部内で激しいイジメに遭い自主退学する。失意のまま都立T校に編入した陽一だが、個性的なクラスメイトと出会い、弱小バスケ部を背負って立つことに―。連戦連敗の雑草集団が最強チームとなって活躍する痛快ベストセラー青春小説。

<a href="books/pages/hashire1.png" target="_blank"><img src="books/pages/hashire1.jpg" alt="Page scan of the book 走れ！Ｔ校バスケット部" width="500rem" height="auto"></a>
<a href="books/pages/hashire2.png" target="_blank"><img src="books/pages/hashire2.jpg" alt="Page scan of the book 走れ！Ｔ校バスケット部" width="500rem" height="auto"></a>

*My Thoughts:* **6/10 reading difficulty**.  
Most unknown words or expressions are understandable from context. Lots of **sports related words**, especially from basketball. Seems to be aimed at high schoolers.  
**No Furigana, only for rare words.**  
It is easy to read, no huge metaphors or anything, but rather **many casual interjections and expressions that you won't easily find** in textbooks, children's books or more serious books. Most of them felt like noone would say them in real life, but I don't really know.  
Characters are often talking and the spotlight of the story changes between several members of the basketball team. This felt a lot better than just following some main star player. Instead we got introduced to many different conflicts and goals, which brings in a lot of variety and also makes it easier to read in many sessions. This is the first sports novel I have read and I dreaded getting bored by lengthy descriptions of game plans or minute details during the game. Those parts turned out very good, with great pacing and engagement. Overall some character interactions feel way too goofy or edgy, like some parts are written by someone who mentally got stuck in high school, but they are not enough to hurt the reading experience.
**How happy am I that I read this?: 7/10**

*Words:* [~580](./words/converted/走れ！Ｔ校バスケット部.txt)  
*Decks:* (~490 words) [No Media](./decks/no-media/book-走れ！Ｔ校バスケット部-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-走れ！Ｔ校バスケット部-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-走れ！Ｔ校バスケット部-allmedia.apkg)  

* * * * * * * * * *

### 火の鳥
![Cover of the book 火の鳥](/books/cover/phoenix.jpg "Cover of the book 火の鳥")  
**Title:** 火の鳥  
**Subtitle:** 鳳凰編
**Author:**  手塚 治虫, 山崎 晴哉 (Tezuka Osamu, Yamazaki Haruya)  
**ISBN:** 978-4041641026 / 4041641020  
**Publisher:** 角川 (KADOKAWA)  
**Release Year:** 1986  
**Pages:** 312  

*Blurb:* 少年は隻腕隻眼であった。裏切りを食み憎しみを吐いて育ち、若くして盗賊の首領となる。その名を我王という。残虐非道な男であった。が、あるとき掌上のテントウ虫に慈悲を与え、これが少女速魚との出会いになった。さて、我王に仏師の命たる右腕を切られたのが茜丸である。彼は我王を呪いつつも天皇の命により伝説の鳳凰を探し求め、ブチと遭い仏に仕える事を知ってゆく。やがて波乱の時が巡り幾多の死が過ぎ、我王は仏師となり、この世を問う。こうして2人の“生”は数奇な因果の調べを奏でるのである。

<a href="books/pages/phoenix1.png" target="_blank"><img src="books/pages/phoenix1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/phoenix2.png" target="_blank"><img src="books/pages/phoenix2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* **Often 9/10 reading difficulty** with many archaic or otherwise unusual words, kanji and grammar, which makes some pages really hard to read and even understand. They seem to be conversations involving royal or political persons from the time the story is set in, ~1300 years ago. This is especially harsh with the very first few pages, which can be overwhelming at first.  
**Most of those very difficult words have Furigana, and not even once, but added at every other appearance, even multiple times for the same word (or name) on the same page.**  
Outside of these pages, **whenever we follow the protagonist, the difficulty is more like a 4/10**, with some rare old rural seeming words that are roughly understandable from context, often with Furigana, but often I was surprised how many very easy words (at least from today's perspective) have Furigana, while other obscurer ones don't.  
During those instances the story quickly moves forward, without any lengthy descriptions of what's happening. I assume that it is like that because the novel is based on the manga from Osamu Tezuka and the author did not insert his own interpretations of the surroundings or feelings or anything else that you would usually find in a novel, but not in a manga. The story revolving around a few characters is nothing groundbreaking, but definitely a nice read. Sadly the frequently appearing few difficult pages that throw around political games and names, soured the overall reading experience for me, since there are too many arachaic words at once to look up and too many names to remember to even understand anything.  
**How happy am I that I read this?: 3/10**.

By the way there seems to be no other animation of the original work other than the OVA — I watched it and it is really bad, a disgrace to the source material, with poor selection and reinterpretation of scenes to fit into the limited one hour runtime.

*Words:* [~950](./words/converted/火の鳥.txt) <!-- *Decks:* [No Media](./decks/no-media/book-火の鳥-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-火の鳥-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-火の鳥-allmedia.apkg) -->   
*Decks:* (~850 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### 英語ができない私をせめないで！ — Again!
![Cover of the book 英語ができない私をせめないで！](/books/cover/semenaide.jpg "Cover of the book 英語ができない私をせめないで！")  
**Title:** 英語ができない私をせめないで！  
**Author:** 小栗 左多里 (Oguri Saori)  

Check my [earlier entry](#英語ができない私をせめないで) on this book for more infos.

<a href="books/pages/semenaide3.png" target="_blank"><img src="books/pages/semenaide3.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/semenaide4.png" target="_blank"><img src="books/pages/semenaide4.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* **4/10 reading difficulty**.  
So I reread this book. It was fun and this time I even collected some words that I still did not know. It is a very fast read, especially since the font is fairly big and the lower fifth of each text page has either an image or is left blank. Still an easy to pick up banger book of a language learner's experiences, nothing much to take away here for your own language learning endeavors, still fun and relatable and never boring to read.  
**How happy am I that I reread this?: 8/10**  
[2023 08-09]

*Words:* [~110](./words/converted/英語ができない私をせめないで.txt)  
*Decks:* (~115 words) [No Media](./decks/no-media/book-英語ができない私をせめないで！-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-英語ができない私をせめないで！-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-英語ができない私をせめないで！-allmedia.apkg)  

* * * * * * * * * *

### コンビニ人間
![Cover of the book コンビニ人間](/books/cover/konbini.jpg "Cover of the book コンビニ人間")  
**Title:** コンビニ人間  
**Author:**  村田 沙耶香 (Murata Sayaka)  
**ISBN:** 978-4167911300 / 4167911302  
**Publisher:** 文藝春秋 (Bungeishunju)  
**Release Year:** 2018  
**Pages:** 168  

*Blurb:* 「いらっしゃいませー!」お客様がたてる音に負けじと、私は叫ぶ。古倉恵子、コンビニバイト歴18年。彼氏なしの36歳。日々コンビニ食を食べ、夢の中でもレジを打ち、「店員」でいるときのみ世界の歯車になれる。ある日婚活目的の新入り男性・白羽がやってきて…。現代の実存を軽やかに問う第155回芥川賞受賞作。

<a href="books/pages/konbini1.png" target="_blank"><img src="books/pages/konbini1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/konbini2.png" target="_blank"><img src="books/pages/konbini2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* **6/10 reading difficulty**.  
Many **words related to work, konbinis, and human interaction**.  
Not many unknown words for me when I read this, rarely not understandable from context. Yet many harder words that I only learned recently from other books. Seems to be aimed at adults.  
**Furigana only for words that use non-Jouyou-Kanji.**  
Enjoyable read, really good pacing, all from the perspective of protagonist Keiko. Plotwise as expected nothing crazy. Just the life of a socially slightly anormal middle-aged woman who started part-time work at a Konbini as a student and now already spent half her life doing nothing but this job. While most family and friends outside of work seem to pity her for treading in place, still without a husband or any career, the protagonist enjoys living this orderly, honest, quiet life. Yet she still struggles with fitting in and the meaning of being a "normal person", or if it is even important. Her orderly life starts to change in unexpected ways when a sleezy lazy jerk is hired during a worker shortage in her konbini.  
**How happy am I that I read this?: 7/10**  
[2023 09]

*Words:* [~270](./words/converted/コンビニ人間.txt) <!-- *Decks:* [No Media](./decks/no-media/book-コンビニ人間-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-コンビニ人間-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-コンビニ人間-allmedia.apkg) -->   
*Decks:* (~260 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### センセイの鞄
![Cover of the book センセイの鞄](/books/cover/sensei.jpg "Cover of the book センセイの鞄")  
**Title:** センセイの鞄  
**Author:** 川上 弘美 (Kawakami Hiromi)  
**ISBN:** 978-4167631031 / 4167631032  
**Publisher:** 文藝春秋 (Bungeishunju)  
**Release Year:** 2004  
**Pages:** 278  

*Blurb:* 駅前の居酒屋で高校の恩師と十数年ぶりに再会したツキコさんは、以来、憎まれ口をたたき合いながらセンセイと肴をつつき、酒をたしなみ、キノコ狩や花見、あるいは島へと出かけた。歳の差を超え、せつない心をたがいにかかえつつ流れてゆく、センセイと私の、ゆったりとした日々。谷崎潤一郎賞を受賞した名作。

<a href="books/pages/sensei1.png" target="_blank"><img src="books/pages/sensei1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/sensei2.png" target="_blank"><img src="books/pages/sensei2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:*
**8/10 reading difficulty**.  
Some **words related to drinking bars (Izakaya) and nature**.
**Furigana for barely any word.**  
TODO (my thoughts on the story).  
Often there will be words that are usually written using kana, which are written in the book with kanji and no furigana at all. Rarely some knowledge of radicals and the context can help in guessing what it is, but when suddenly the protagonist thought that there would be less 塵, I had no idea that it is just 'trash', as in ごみ. This makes reading the book unnecessarily annoying and I do not understand what the intention behind this is, other than alienating people who are no experts with kanji. I know this book was first published in 2004 and apparently until 2010 officially there were the Touyou-Kanji, not the Jouyou-Kanji, but neither contain this and other obscure kanji that just appear without furigana in this book. Unnecessarily sours the experience, making me ignore these words. Other examples: 箪笥, お洒落, 蟹, 苔, 籠, 蠅, 茄子, 黴, 従兄弟, 海苔, 蟻, 頁, 林檎, 玉葱, 蛸, 鴨.  
**How happy am I that I read this?: 4/10**  
[2023 09-10]

*Words:* [~750](./words/converted/センセイの鞄.txt) <!-- *Decks:* [No Media](./decks/no-media/book-センセイの鞄-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-センセイの鞄-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-センセイの鞄-allmedia.apkg) -->   
*Decks:* (~715 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### にわか魔女のタマユラさん
![Cover of the book にわか魔女のタマユラさん](/books/cover/witch.jpg "Cover of the book にわか魔女のタマユラさん")  
**Title:** にわか魔女のタマユラさん  
**Author:** 伊藤 充子 (Itou Mitsuko)  
**ISBN:** 978-4035011606 / 4035011606  
**Publisher:** 偕成社 (KAISEI-SHA)  
**Release Year:** 2022  
**Pages:** 160  

*Blurb:* タマユラさんは魔女初心者ですが、町のひとや動物たちの悩みを解決するために、つかい魔や魔法道具たちと協力して一生けんめいがんばります。わたしになんか無理！　と思うようなことでも、やってみないとわかりません。

<a href="books/pages/witch1.png" target="_blank"><img src="books/pages/witch1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/witch2.png" target="_blank"><img src="books/pages/witch2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:*
**2/10 reading difficulty**.  
Aimed at very young school children who can read the most basic kanji.  
**Furigana for all Kanji except for absolute beginner words.**  
Very straightforward, sutiable for bedtime stories. Each chapter consists of a mini-adventure of the protagonist. She is a cafe owner who gets magical powers that allow her to talk to things and animals. All of these adventures are completely inconsequential without the slightest bit of suspense or mystery -- as can be expected from the looks of this book. Still nice to read a book on this level for a change. Harder than the usual results one gets for "childrens' books", but still easy to read. Sometimes some words can even be harder to quickly read without kanji. The end of the story is very nice and cute.  
**How happy am I that I read this?: 7/10**  
[2023 10]

*Words:* [~75](./words/converted/にわか魔女のタマユラさん.txt)
*Decks:* (~70 words) [No Media](./decks/no-media/book-にわか魔女のタマユラさん-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-にわか魔女のタマユラさん-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-にわか魔女のタマユラさん-allmedia.apkg)  

* * * * * * * * * *

### 白河夜船
![Cover of the book 白河夜船](/books/cover/asleep.jpg "Cover of the book 白河夜船")  
**Title:** 白河夜船  
**Author:**  吉本 ばなな (Yoshimoto Banana)  
**ISBN:** 978-4101359175 / 4101359172  
**Publisher:** 新潮社 (SHINCHOSHA)  
**Release Year:** 2002  
**Pages:** 194  

*Blurb:* いつから私はひとりでいる時、こんなに眠るようになったのだろう―。植物状態の妻を持つ恋人との恋愛を続ける中で、最愛の親友しおりが死んだ。眠りはどんどん深く長くなり、うめられない淋しさが身にせまる。ぬけられない息苦しさを「夜」に投影し、生きて愛することのせつなさを、その歓びを描いた表題作「白河夜船」の他「夜と夜の旅人」「ある体験」の“眠り三部作”。定本決定版。

<a href="books/pages/asleep1.png" target="_blank"><img src="books/pages/asleep1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/asleep2.png" target="_blank"><img src="books/pages/asleep2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/asleep3.png" target="_blank"><img src="books/pages/asleep3.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:*
**6/10 reading difficulty**.  
Aimed at adults.  
**Furigana only for words that use non-Jouyou-Kanji.**  
The book is separated into three stories, each around 75, 70, 40 pages long, which share the common theme of 'sleeping', but other than that are completely independent from each other. Even the characters are different.  
The protagonist in the first chapter keeps sleeping too much after she lost a close friend and just can't really get motivated to do things.  
In the second chapter the protagonist and a close family member mourn the loss of another family member and stay awake most nights.  
Last chapter is quite short with the protagonist barely able to sleep without getting piss drunk before, soon realizing she is missing an old romantic rival.  
For the most part I felt like the book was either kinda boring or I missed some metaphors or other nuances between the lines. One could think it is because the stories themselves are kinda short, but then again i rank the longest one as the worst one -- so ranking the three chapters: 2, 3, 1. The second chapter was interesting enough to keep me hooked, but the ending was kinda meh. The third chapter also had intersting parts that stood out on its own, but again the story was nothing special and the conclusion was ok. The first chapter is just dragging along, which is why the first chapter feels like I missed some deeper meaning, although it was not at all obvious that I am missing things or not understanding stuff. Maybe I am just not in the target audience.  
**How happy am I that I read this?: 4/10**  
[2023 10-11]

*Words:* [~370](./words/converted/白河夜船.txt) <!-- *Decks:* [No Media](./decks/no-media/book-白河夜船-nomedia.apkg) | [No Pictures](./decks/no-pictures/book-白河夜船-nopictures.apkg) | [All Media ⭐](./decks/all-media/book-白河夜船-allmedia.apkg) -->   
*Decks:* (~365 words) ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~  

* * * * * * * * * *

### あの日の「徹子の部屋」
![Cover of the book あの日の「徹子の部屋」](/books/cover/tetsuko.jpg "Cover of the book あの日の「徹子の部屋」")  
**Title:** あの日の「徹子の部屋」  
**Author:** 黒柳 徹子 (Kuroyanagi Tetsuko)  
**ISBN:** 978-4022619280 / 4022619287  
**Publisher:** 朝日新聞出版 (Asahi Shimbun Publications)  
**Release Year:** 2018  
**Pages:** 408  

*Blurb:* 1976年の番組開始から翌年にかけて放送された回より、ゲスト16人分を文庫に。憧れの先輩俳優、作家、音楽家、歌手など、さまざまなジャンルの著名人を相手に、時に爆笑、時に涙しながら、真剣にゲストに向き合う徹子さん。今ではもう聞くことのできない貴重な対談集。

<a href="books/pages/tetsuko.png" target="_blank"><img src="books/pages/tetsuko.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* **This book is so boring**. I read a bit more than 100 pages (5 chapters) and just decided that this book is not for me, making this the first book I will not finish. Each chapter is pretty much a transcript of the talks the host Tetsuko had with a guest. The chapters are supposed to be kind of a best-of of guests, so I kept reading and reading but all of them had nothing interesting to say. Maybe it would be more interesting if I knew those celebrities at all, maybe I am missing a bunch of hidden meaning because of cultural or language barriers. In the end every conversation felt like such a drag. Like something I wouldn't mind too much running on the TV in the background, but actively consuming it is a pain. This book is probably for nostalgic fans of this talk show or for people who just couldn't wait to read a talk show, but man this is definitely not me. Anyway, as far as I can tell the language is mostly not too deep and by nature casual, but still there are some words I marked and will provide here, at least for the pages I read.  
**How happy am I that I read a quarter of this?: 1/10**  
[2023 11]

* * * * * * * * * *

### 護られなかった者たちへ
![Cover of the book 護られなかった者たちへ](/books/cover/protected.jpg "Cover of the book 護られなかった者たちへ")  
**Title:** 護られなかった者たちへ  
**Author:** 中山 七里 (Nakayama Shichiri)  
**ISBN:** 978-4299006332 / 429900633X  
**Publisher:** 宝島社 (TAKARAJIMASHA)  
**Release Year:** 2022  
**Pages:** 477  

*Blurb:* 仙台市の福祉保健事務所課長・三雲忠勝が、手足や口の自由を奪われた状態の餓死死体で発見された。三雲は公私ともに人格者として知られ怨恨が理由とは考えにくい。一方、物盗りによる犯行の可能性も低く、捜査は暗礁に乗り上げる。三雲の死体発見から遡ること数日、一人の模範囚が出所していた。男は過去に起きたある出来事の関係者を追っている。男の目的は何か?なぜ、三雲はこんな無残な殺され方をしたのか?罪と罰、正義が交錯した先に導き出されるのは、切なすぎる真実―。

<a href="books/pages/protected1.png" target="_blank"><img src="books/pages/protected1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/protected2.png" target="_blank"><img src="books/pages/protected2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* [...] interesting crime thriller so far. TODO  
TODO  
**How happy am I that I read this?: 6/10**  
[2023 12 - 2024 02]

* * * * * * * * * *
* * * * * * * * * *

Currently reading:

### 消えた初恋
![Cover of the book 消えた初恋](/books/cover/vanished-love.jpg "Cover of the book 消えた初恋")  
**Title:** 消えた初恋  
**Author:**  宮田 光, ひねくれ 渡 (Miyata Hikaru, Hinekure Wataru?)  
**ISBN:** 978-4086804158 / 4086804158  
**Publisher:** 集英社 (SHUEISHA)  
**Release Year:** 2021  
**Pages:** 256  

*Blurb:* 片思い中の橋下さんに消しゴムを借りた男子高校生の青木。ところが消しゴムには両思いになれるおまじないとしてクラスメイトの男子・井田の名前が…。玉砕した青木の初恋。しかし消しゴムを見た井田に勘違いされてしまい？青木は橋下さんが、橋下さんは井田が好き。なのに「友達から始めないか？」物語は想定外の方向に転がりはじめ!?ドラマ化コミックスの小説版！

<a href="books/pages/vanished-love1.png" target="_blank"><img src="books/pages/vanished-love1.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>
<a href="books/pages/vanished-love2.png" target="_blank"><img src="books/pages/vanished-love2.jpg" alt="Page scan of the book 火の鳥" width="500rem" height="auto"></a>

*My Thoughts:* [...] funny and entertaining easy read. TODO  
TODO  
**How happy am I that I read this?: 9/10**  
[2024 02-03]

* * * * * * * * * *
* * * * * * * * * *

I haven't read the following books yet, but they are ready on my shelf:

### おしゃべりな部屋
![Cover of the book おしゃべりな部屋](/books/cover/chatty.jpg "Cover of the book おしゃべりな部屋")  
**Title:** おしゃべりな部屋  
**Author:** 川村 元気, 近藤 麻理恵 (Kawamura Genki, Kondo Marie)  
**ISBN:** 978-4120055102 / 4120055108  
**Publisher:** 中央公論新社 (CHUOKORON-SHINSHA)  
**Release Year:** 2022  
**Pages:** 218  

* * * * * * * * * *

### おやすみ、東京
![Cover of the book おやすみ、東京](/books/cover/goodnight.jpg "Cover of the book おやすみ、東京")  
**Title:** おやすみ、東京  
**Author:** 吉田 篤弘 (Yoshida Atsuhiro)  
**ISBN:** 978-4-7584-4291-6  
**Publisher:** 角川春樹事務所 (KADOKAWA HARUKI)  
**Release Year:** 2019  
**Pages:** 317  

*Blurb:* 東京、午前一時。この街の人々は、自分たちが思っているよりはるかに、さまざまなところ、さまざまな場面で誰かとすれ違っているー映画会社で“調達屋”をしているミツキは、ある深夜、「果物のびわ」を午前九時までに探すよう頼まれた。今回もまた夜のタクシー“ブラックバード”の運転手松井に助けを求めたが…。それぞれが、やさしさ、淋しさ、記憶と夢を抱え、つながっていく。月に照らされた東京を舞台に、私たちは物語を生きる。幸福な長篇小説。滋味深く静かな温もりを灯す、１２の美味しい物語。

* * * * * * * * * *

### ナミヤ雑貨店の奇蹟
![Cover of the book ナミヤ雑貨店の奇蹟](/books/cover/namiya-wonder.jpg "Cover of the book ナミヤ雑貨店の奇蹟")  
**Title:** ナミヤ雑貨店の奇蹟  
**Author:** 東野 圭吾 (Higashino Keigo)  
**ISBN:** 978-4-04-101451-6 / 4041014514  
**Publisher:** 角川 (KADOKAWA)  
**Release Year:** 2012  
**Pages:** 413  

*Blurb:* 悪事を働いた3人が逃げ込んだ古い家。そこはかつて悩み相談を請け負っていた雑貨店だった。廃業しているはずの店内に、突然シャッターの郵便口から悩み相談の手紙が落ちてきた。時空を超えて過去から投函されたのか?3人は戸惑いながらも当時の店主・浪矢雄治に代わって返事を書くが…。次第に明らかになる雑貨店の秘密と、ある児童養護施設との関係。悩める人々を救ってきた雑貨店は、最後に再び奇蹟を起こせるか!?

* * * * * * * * * *

### 吾輩は猫である
![Cover of the book 吾輩は猫である](/books/cover/cat.jpg "Cover of the book 吾輩は猫である")  
**Title:** 吾輩は猫である  
**Author:** 夏目 漱石 (Natsume Soseki)  
**ISBN:** 978-4087520477 / 4087520471  
**Publisher:** 集英社 (SHUEISHA)  
**Release Year:** 1906 (2009)  
**Pages:** 329  

*Blurb:* 「吾輩は猫である。名前はまだない。」苦沙弥先生の家に拾われた猫の「吾輩」から見れば、人間社会はこっけいそのもの。無名猫の視点から、軽妙酒脱な文体にのせて放たれる文明批評と渋いウイットは時代を超えて読者の心をつかんできた。見識とシャレ気あふれる漱石の永遠のエンターテインメント文学。

* * * * * * * * * *

### 源氏物語
![Cover of the book 源氏物語](/books/cover/genji.jpg "Cover of the book 源氏物語")  
**Title:** 源氏物語  
**ISBN:** 978-4807222148 / 4807222147  
**Publisher:** 舵社 ([デカ文字文庫](https://www.kazi.co.jp/public/book/dekamozi/deka.html))  
**Pages:** 214  



## Games

Some of the games I have played in Japanese that had significant amounts of text with interesting new vocabulary. Sorted by the order in which I played them. The games will not be rated by their quality, but I will give a kinda arbitrary rating similar to the books, but depending on how happy I was that I played the game in Japanese, i.e. how much fun or learning value I feel like I got from it. This will tend to be high since I am far better at finding good games than good books.

* * * * * * * * * *

### ファイナルファンタジーVII
![Cover of the game ファイナルファンタジーVII](/games/cover/ff7.jpg "Cover of the game ファイナルファンタジーVII")  
**Title:** ファイナルファンタジーVII  
**English Title**: Final Fantasy VII  
**Developer:** Square Enix (Japan)  
**Release Year**: 1997  
**Platform**: PS1, with Ports on many other (Switch, PC, Playstation, Xbox, ...)  
**Region**: Japanese texts included in Switch EU eShop version, change system language before starting the game (at least on Switch).  
**More Info**: No voiceover; no log; no Furigana.  

<a href="games/screenshots/ff71.jpg" target="_blank"><img src="games/screenshots/ff71-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/ff72.jpg" target="_blank"><img src="games/screenshots/ff72-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/ff73.jpg" target="_blank"><img src="games/screenshots/ff73-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/ff74.jpg" target="_blank"><img src="games/screenshots/ff74-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>

*My Thoughts:* Pretty standard Japanese most of the time. Kanji use seems to be suitable for teenagers, with many words written in Kana. I can recommend playing one of the Remaster versions (e.g. Switch, Steam etc) that can fast forward slow sections of the game or even give you cheat-like powers for the battles.  
**How happy am I that I played this in Japanese?: 8/10**  

*Words:* [~80](./words/ファイナルファンタジーVII.txt) <!-- *Decks:* [No Media](./decks/no-media/game-ファイナルファンタジーVII-nomedia.apkg) | [No Pictures](./decks/no-pictures/game-ファイナルファンタジーVII-nopictures.apkg) | [All Media ⭐](./decks/all-media/game-ファイナルファンタジーVII-allmedia.apkg) -->   
*Decks:* ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### スーパーマリオRPG (Remake)
![Cover of the game スーパーマリオRPG](/games/cover/super-mario-rpg.jpg "Cover of the game スーパーマリオRPG")  
**Title:** スーパーマリオRPG  
**English Title**: Super Mario RPG  
**Developer:** Nintendo (Japan)  
**Release Year**: 2023 (original: 1996)  
**Platform**: Nintendo Switch (original: SNES)  
**Region**: Japanese texts included in EU version, in-game language selection.  
**More Info**: No voiceover; no log; barely any Kanji, all with Furigana.  

<a href="games/screenshots/super-mario-rpg1.jpg" target="_blank"><img src="games/screenshots/super-mario-rpg1-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/super-mario-rpg2.jpg" target="_blank"><img src="games/screenshots/super-mario-rpg2-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/super-mario-rpg3.jpg" target="_blank"><img src="games/screenshots/super-mario-rpg3-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>
<a href="games/screenshots/super-mario-rpg4.jpg" target="_blank"><img src="games/screenshots/super-mario-rpg4-small.jpg" alt="In-game screenshot from スーパーマリオRPG" width="500rem" height="auto"></a>

*My Thoughts:* Super easy Japanese most of the time for plot relevant dialogue. Some more obscure expressions or words during optional dialogue like when talking to NPCs in towns or when reading the minds of enemies. There are barely any kanji, which sometimes makes it a bit harder to read. Overall fun reading times and nice short RPG (~15-25h).  
**How happy am I that I played this in Japanese?: 9/10**  
[2023 11]

*Words:* [~20](./words/スーパーマリオRPG.txt) <!-- *Decks:* [No Media](./decks/no-media/game-スーパーマリオRPG-nomedia.apkg) | [No Pictures](./decks/no-pictures/game-スーパーマリオRPG-nopictures.apkg) | [All Media ⭐](./decks/all-media/game-スーパーマリオRPG-allmedia.apkg) -->   
*Decks:* ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### ハーデス
![Cover of the game ハーデス](/games/cover/hades.jpg "Cover of the game ハーデス")  
**Title:** ハーデス  
**English Title**: Hades  
**Developer:** Supergiant Games (USA)  
**Release Year**: 2020  
**Platform**: Multi (Switch, PC, Playstation, Xbox, ...)  
**Region**: Japanese texts included in EU version, in-game language selection.  
**More Info**: Only English voiceover, but extremely high quality; High quality Japanese translation; no Furigana; no Log.  

*My Thoughts:* TODO  
**How happy am I that I played this in Japanese?: 9/10**  
[2023 11-12]

*Words:* [WIP](./words/unfinished-ハーデス.txt) <!-- *Decks:* [No Media](./decks/no-media/game-ハーデス-nomedia.apkg) | [No Pictures](./decks/no-pictures/game-ハーデス-nopictures.apkg) | [All Media ⭐](./decks/all-media/game-ハーデス-allmedia.apkg) -->   
*Decks:* ~~[No Media]~~ | ~~[No Pictures]~~ | ~~[All Media]~~

* * * * * * * * * *

### ザ・メッセンジャー
![Cover of the game ザ・メッセンジャー](/games/cover/messenger.jpg "Cover of the game ザ・メッセンジャー")  
**Title:** ザ・メッセンジャー  
**English Title**: The Messenger  
**Developer:** Sabotage Studio (Canada)  
**Release Year**: 2018  
**Platform**: Multi (Switch, PC, PS4, Xbox One)  
**Region**: Japanese texts included in EU version, in-game language selection.  
**More Info**: No Furigana; no Log, but many conversations can be directly repeated.  

*My Thoughts:* TODO
**How happy am I that I played this in Japanese?: 7/10**  
[2023 12-01]

* * * * * * * * * *
* * * * * * * * * *

Currently playing:


### 大逆転裁判
![Cover of the game 大逆転裁判](/games/cover/great-attorney.jpg "Cover of the game 大逆転裁判")  
**Title:** 大逆転裁判  
**English Title**: The Great Ace Attorney  
**Developer:** Capcom (Japan)  
**Release Year**: 2015, 2021  
**Platform**: 3DS, Switch  
**Region**: Apparently no Japanese texts outside of Asian versions.  
**More Info**: No voiceover; Log.  



* * * * * * * * * *
* * * * * * * * * *

I haven't played the following games yet, but they are ready on my shelf:

* * * * * * * * * *

### ペルソナ5 ザ・ロイヤル
![Cover of the game ペルソナ5 ザ・ロイヤル](/games/cover/persona5-royal.jpg "Cover of the game ペルソナ5 ザ・ロイヤル")  
**Title:** ペルソナ5 ザ・ロイヤル  
**English Title**: Persona 5 Royal  
**Developer:** Atlus (Japan)  
**Release Year**: 2020 (original: 2016)  
**Platform**: Multi (Switch, Playstation, Xbox, PC, ...)  
**Region**: Apparently no Japanese texts outside of Asian versions.  
**More Info**: ...  

* * * * * * * * * *

### バディミッション BOND
![Cover of the game バディミッション BOND](/games/cover/buddy-mission-bond.jpg "Cover of the game バディミッション BOND")  
**Title:** バディミッション BOND  
**English Title**: Buddy Mission Bond  
**Developer:** Ruby Party (Japan)  
**Release Year**: 2021  
**Platform**: Switch  
**Region**: Japan Release only.  
**More Info**: Manga-like Visual Novel; all dialogue voiced; Push to Continue; Log with replayable audio. Also check out [Game Gengo](https://www.youtube.com/@GameGengo)'s video [Learn Japanese with Buddy Mission Bond!『バディミッション BOND』 Vocabulary Series](https://www.youtube.com/watch?v=oIPBoW8cFVA).  

* * * * * * * * * *

### ファミコン探偵倶楽部 (Remake)
![Cover of the game ファミコン探偵倶楽部](/games/cover/famicom-detectives.jpg "Cover of the game ファミコン探偵倶楽部")  
**Title:** ファミコン探偵倶楽部  
**English Title**: Famicom Detective Club  
**Developer:** Nintendo (Japan)  
**Release Year**: 2021  
**Platform**: Switch  
**Region**: No EU physical version, only download. I got the Japanese physical version.  
**More Info**: all dialogue voiced; Push to Continue; Log with replayable audio.  




# Notes on Anki Decks

## No Media, No Pictures, All Media

For most Anki decks I create, they come in different versions, with different file sizes:
- "**No Media**" (no images, no audio, ~0.5MB)
- "**No Pictures**" (no images, with audio, ~10MB)
- "**All Media**" (with images, with audio, ~50MB)
  - with better image selection if there is a star emoji "⭐", see [below](#all-images-deck-with-a-star-emoji-⭐)

If you want to check out a deck, you can quickly download and add the "No Media"-deck and later just import the "No Pictures"- or the "All Media"-deck, which will add the media to your existing deck. Anki automatically updates your notes. This will only work for notes that you have not manually changed. For those, you can do one of three things:
- sort by "modified date" in your deck and manually update these notes that did not get updated with the media
- open the new deck in a separate profile and there change all of them at once, e.g. by adding something like a single letter to the front card. And then remove it again. This way, there is no actual change, but all the notes will have their "modified date" updated to just now.
- export the new deck as a csv, import the csv back into the old deck. Have not tested this approach, but it should work some way 👀

When I create a deck, most words have the following extra stuff, saved in the note type's fields specified in brackets:

1. Japanese **Example Sentences** (Examples, ExampleJapanese)
2. **Pitch Accent** Pattern (pitchPattern)
3. **Audio** file for the pronunciation (audioJP)
4. **Images** (pictures)

All decks contain the example sentences (one field with Japanese and its English translation, one field with Japanese only) and the pitch accent pattern (though it looks like an image, it is acutally just HTML and CSS, so it does not use much space), since it barely takes up any extra space.

For example in the decks for [祟りのゆかりちゃん](#祟りのゆかりちゃん), the deck without any of the above extras would take up ~60kB. Adding the pitch pattern will add ~130kB. Adding the example sentences will add ~55kb. Which is why I decided that the smallest decks I offer here will include the pitch pattern and the example sentences, since they barely take up any space. In contrast, the audio takes up an additional 10MB in that deck. The images another 45MB. For other decks, all this additional space will probably be larger, since more common words will get more example sentences, pitch accent data, audio files and images files, while rarer words often have these fields completely empty.

### "All Images"-Deck with a star emoji "⭐"

For all the decks that were created until the first half of 2023, there were usually up to 4 images with sometimes very poor relevance to the actual word. This is because I just loaded 4 images per word with the Add-On "[Japanese Example Sentences](https://ankiweb.net/shared/info/2413435972)". For rather intagible words, this often leads to images that just contain the word, like this:

![image search result of the word たとえばなし which is pretty useless](/stuff/images/tatoe.png "Picture showing the word たとえ話 and its reading たとえばなし.")  

When I finished the deck for [祟りのゆかりちゃん](#祟りのゆかりちゃん), which I had only started before up until a fifth of all the words, I initially added more images and only kept the most useful ones, if there were any, as far as I could judge their usefulness. I really liked the result, so since then I do this for all new decks. Those "All Media"-decks are marked with a "⭐", so they have manually filtered images for each word.

I did not create the decks for the books in the order in which they are listed above. The first one to get better images was [祟りのゆかりちゃん](#祟りのゆかりちゃん), aside from the first fifth of words. All books that come after, will also have better images. But of those before, these also have better images: [ガールズ・ブルー](#ガールズ・ブルー), [１００億人のヨリコさん](#１００億人のヨリコさん), and [二度のお別れ](#二度のお別れ).

More about how I use these Add-Ons in [a chapter below](https://ankiweb.net/shared/info/2413435972).


## Design Choices
Feel free to adjust my note types any way you like and to reuse them. I am not particularly fond of any design choices. Some of it was haphazardly added, like some JavaScript code that handles how the Japanese word's dictionary format, expression, and reading are shown. Currently there is nothing I would like to change, but write me an e-mail if you have any suggestions or questions: [alex.mai@posteo.net](mailto:alex.mai@posteo.net)

How the decks are created is described in the [next chapter](#anki-deck-creation).



# Anki Deck Creation

TODO assumption: words in a text file, sync before doing stuff

## Saving Words to Anki
TODO Rikai-Sama, Firefox Import Add-On "[Real-Time Import](https://ankiweb.net/shared/info/1659361456)" and Settings, text file in Firefox

## Deduplication
Using the Firefox-Add-On to save words to Anki, duplicates will not be saved in the first place. In Anki the first field, which is in my case the dictionary form of the word, is used to check for duplicates, ignoring the other fields.

But all cards that I created since the middle of 2023, have been created in a separated profile of Anki, where no other decks were present. So duplicates were only automatically not saved, if they were duplicates within the same deck. I changed to this style, so that more of the originally written down words are included in the decks. Since my studied vocab is usually behind around 5 books of my reading progress, there will be many duplicates between books.

Once I then import these new decks into my private profile, I check for all duplicate cards using "Notes -> Find Duplicates" in the Anki Browser. If one of the duplicates is an old card and the new card was created at a time that the old one already existed, then I consider deleting the old one, since I clearly didn't remember it when I saw the word in the book.


## Final Touch: Anki Add-Ons

The example sentences are added in batch with the Anki Add-On "[Japanese Example Sentences](https://ankiweb.net/shared/info/2413435972)". Many cards won't have examples, since the Add-On won't find any example sentences for uncommon words.

Pitch accent is added in batch with the Anki Add-On "[Japanese Pitch Accent](https://ankiweb.net/shared/info/148002038)". Sometimes it will be empty, if the Add-On can't find anything. Rarely the Add-On will load in the wrong reading of the word or will straight up be the wrong word, in which case I delete the pitch accent image. Note that the pitch accent loaded into the card by the add-on can still be wrong, as sometimes it seems to not be the same as the pitch used in the audio recording. But then again, the audio loaded by the RikaiSama Firefox-Add-On might be wrong here, who knows — this is a rare case, you should just know that it can happen.

TODO "[Batch Download Pictures From Google Images](https://ankiweb.net/shared/info/561924305)"


## Creating the Deck Versions

TODO "Notes -> Find and Replace", there check "Treat input as regular expression", find ".*" and replace it with nothing, with the proper field selected to make it empty.

